FROM ubuntu:latest
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get clean && apt-get update && apt-get install -y locales
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

LABEL maintainer="Fernando Roa <froao@unal.edu.co>"

RUN apt-get -y update && apt-get install -y --no-install-recommends \
    sudo \
    python3 \
    build-essential \
    r-base \
    python3-pip \
    python3-setuptools \
    python3-dev \
    libcurl4-gnutls-dev \
    libcairo2-dev \
    libxt-dev \
    libssl-dev \
    libssh2-1-dev \
    libxml2-dev \
    libsasl2-dev \
    wget \
    && rm -rf /var/lib/apt/lists/*
 
RUN wget https://github.com/jgm/pandoc/releases/download/2.13/pandoc-2.13-1-amd64.deb
RUN dpkg -i pandoc-2.13-1-amd64.deb
RUN pandoc --version

RUN pip3 install --upgrade pip

ENV PYTHONPATH "${PYTHONPATH}:/app"

ADD requirements.txt .
RUN pip3 install -r requirements.txt 
RUN [ "python3", "-c", "import nltk; nltk.download('stopwords')" ]
RUN [ "python3", "-c", "import nltk; nltk.download('punkt')" ]
RUN cp -r /root/nltk_data /usr/local/share/nltk_data 

RUN R -q -e "install.packages('rmarkdown',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -q -e "install.packages('leaflet',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -q -e "install.packages('shiny',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -q -e "install.packages('shinydashboard',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -q -e "install.packages('shinyjs',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -q -e "install.packages('dplyr',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -q -e "install.packages('stringr',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -q -e "install.packages('gtools',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -q -e "install.packages('DT',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -q -e "install.packages('data.table',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -q -e "install.packages('mongolite',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -q -e "install.packages('pryr',dependencies=TRUE, repos='http://cran.rstudio.com/')"

RUN echo "local(options(shiny.port = 8080, shiny.host = '0.0.0.0'))" > /usr/lib/R/etc/Rprofile.site

RUN addgroup --system app \
    && adduser --system --ingroup app app

WORKDIR /home/app
COPY app .
RUN chown app:app -R /home/app

USER app
EXPOSE 8080
CMD ["R", "-e", "shiny::runApp('/home/app')"]

