# localMode <- TRUE  # uses csv

#####################################
#
#   COMPLEMENTARY OBJECTS, SMALL TABLES AND LISTS
#
####################################

today_date <- Sys.Date() + 1

ser_list <- readRDS("rds/ser_list.rds")

#####################################
#
#   places, locations
#
####################################

# getwd()
# setwd("/home/fernando/GoogleDrive/ds4a/products/code_and_notebooks/dockerKJ/app")

# col_states          <- readRDS("rds/col_states.rds")
# saveRDS(col_states,"rds/col_states_ascii.rds")
# col_states          <- readRDS("rds/col_states_ascii.rds")
# col_states_coord    <- readRDS("rds/col_states_coord.rds")
# write.csv(col_states_coord,"datasets/col_states_coord.csv")

# dput(col_states)

col_states <- c(
  "Antioquia", "Arauca", "Atlántico", "Bolivar", "Cundinamarca",
  "La Guajira", "Magdalena", "Nariño", "Norte de Santander", "Santander",
  "Valle del Cauca"
)

col_states_coord <- setDF(fread("datasets/col_states_coord.csv", encoding = "UTF-8"))
colombia_neigh <- setDF(fread("datasets/withNeigh_coords.csv", encoding = "UTF-8"))
colombia_locations <- setDF(fread("datasets/colombia_locations.csv", encoding = "UTF-8"))

afr_countries <- readRDS("rds/afr_countries.rds")
afr_locations_coord <- setDF(fread("datasets/afr_locations_coord.csv", encoding = "UTF-8"))
afr_locations <- readRDS("rds/afr_locations.rds")
africa_locations <- setDF(fread("datasets/africa_locations.csv", encoding = "UTF-8"))


# afr_locations_coord <- readRDS("rds/afr_locations_coord.rds")
# write.csv(afr_locations_coord,"datasets/afr_locations_coord.csv")

#####################################
#
#   MAP colors
#
####################################

colorList12 <- c(
  "forestgreen", # 228b22 34 139 34
  "#ee0000", # red2 238 0 0
  "orange", #     255 165 0
  "cornflowerblue", # 6495ed  100 149 237
  "magenta", # ff00ff  255 0 255
  "#6e8b3d", # darkolivegreen4', #6e8b3d 110 139 61
  "indianred1", # ff6a6a 255 106 106
  "tan4", # 8b5a2b 139 90 43
  "darkblue", # 00008b  0 0 139
  "#8b7e66", # wheat4 139 126 102
  "#8b1a1a" # 139 26 26
  , "#99ff99"
) # 153 255 153

names(colorList12) <- ser_list[[2]]

decColorList12 <- c(
  "34, 139, 34",
  "238, 0, 0",
  "255, 165, 0",
  "100, 149, 237",
  "255, 0, 255",
  "110, 139, 61",
  "255, 106, 106",
  "139, 90, 43",
  "0, 0, 139",
  "139, 126, 102",
  "139,26, 26",
  "153, 255, 153"
)

names(decColorList12) <- ser_list[[2]]

#####################################
#
#   columns
#
####################################

dontwrap_cols <- c(
  "idea", "date", "NLP_tag", "location", "created_at_tz", "location_name",
  "city"
  # ,"service_point_name"
  , "Service Point"
)

unwanted_columns <- c(
  "country_name", "satisfied_num",
  "response_type",
  "created_at_tz",
  "unique_id",
  "user_id",
  "state",
  "created_at_tz_posix"
)

first_cols <- c(
  "NLP_tag", "idea", "date", "satisfied", "service_type",
  "service_point_name"
  # ,"Service Point"
)

# remove idea, its presence is mandatory, service_type, lat, lng

mandatory <- c("idea", "service_type", "service_point_name", "unique_id", "lat", "lng")

columns_col <- c(
  "date", "country_name", "satisfied", "response_type",
  "satisfied_num", "created_at_tz", "unique_id",
  "user_id", "is_starred", "city", "state",
  "location", "service_point_name", "neighbourhood"
)

columns_col <- sort(columns_col)

columns_afr <- c(
  "date", "country_name", "location_name", "service_point_name",
  "satisfied", "satisfied_num", "created_at_tz",
  "unique_id", "user_id"
)

columns_afr <- sort(columns_afr)

#
#   LOCALMODE ALMOST ALWAYS FALSE
#

if (localMode) { # usually not loaded
  # getwd()
  col_feed_con_raw_01 <- setDF(fread("datasets/1. Colombia_feedback_consolidado_raw.csv"))

  # View(col_feed_con_raw_01)
  #
  # note, this is a modified dataset with 15 columns
  #
  # [1] "country_name"       "service_type"       "satisfied"          "response_type"
  # [5] "satisfied_num"      "idea"               "created_at_tz"      "unique_id"
  # [9] "user_id"            "is_starred"         "city"               "state"
  # [13] "location"           "service_point_name" "neighbourhood"

  col_feed_con_raw_01$idea <- gsub("(.{1,80})(\\s|$)", "\\1\n", col_feed_con_raw_01$idea)

  # install.packages("bit64")
  africa_feed_con_raw_03 <- setDF(fread("datasets/3. East_Africa_feedback_consolidado_raw.csv"))
  # colnames(africa_feed_con_raw_03)
  #
  # africa_feed_con_raw_03[which(africa_feed_con_raw_03$country_name=="Rwanda" &
  #                                africa_feed_con_raw_03$service_type=="Community Pulse"
  #                              & africa_feed_con_raw_03$location_name=="Gihembe Camp"
  #                              & africa_feed_con_raw_03$satisfied==TRUE),]
  #
  # note, columns:
  #
  # [1] "country_name"       "location_name"      "service_point_name" "service_type"
  # [5] "satisfied"          "satisfied_num"      "idea"               "created_at_tz"
  # [9] "unique_id"          "user_id"

  africa_feed_con_raw_03$idea <- gsub("(.{1,80})(\\s|$)", "\\1\n", africa_feed_con_raw_03$idea)

  col_feed_con_raw_01_coords <- dplyr::left_join(col_feed_con_raw_01,
    colombia_neigh,
    by = c("state", "city", "neighbourhood"),
    all.x = TRUE,
    sort = FALSE
  )

  idx_no_lat <- which(is.na(col_feed_con_raw_01_coords$lat))
  cols_no_coord <- setdiff(colnames(col_feed_con_raw_01_coords), c("lat", "lng"))
  col_feed_con_raw_01_coords[idx_no_lat, ] <- dplyr::left_join(col_feed_con_raw_01_coords[idx_no_lat, cols_no_coord],
    colombia_locations,
    by = c(
      "city", "location", "country_name",
      "state", "service_point_name"
    ),
    all.x = TRUE,
    sort = FALSE
  )[, colnames(col_feed_con_raw_01_coords)]


  africa_feed_con_raw_03_coords <- dplyr::left_join(africa_feed_con_raw_03,
    africa_locations,
    by = c(
      "country_name", "location_name",
      "service_point_name"
    ),
    all.x = TRUE,
    sort = FALSE
  )


  col_feed_con_raw_01_coords$date <- as.Date(sub(
    " UTC", "",
    col_feed_con_raw_01_coords$created_at_tz
  ))

  africa_feed_con_raw_03_coords$date <- as.Date(sub(
    " UTC", "",
    africa_feed_con_raw_03_coords$created_at_tz
  ))
}
