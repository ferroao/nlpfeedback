##############################################
#
#   functions
#
##############################################

colombia_coord_date <- function(dataset) {
  dataset <- dplyr::left_join(dataset,
    colombia_neigh,
    by = c("state", "city", "neighbourhood")
  )
  idx_no_lat <- which(is.na(dataset$lat))
  cols_no_coord <- setdiff(colnames(dataset), c("lat", "lng"))
  dataset[idx_no_lat, ] <- dplyr::left_join(dataset[idx_no_lat, cols_no_coord],
    colombia_locations,
    by = c(
      "city", "location", "country_name",
      "state", "service_point_name"
    )
  )[, colnames(dataset)]

  dataset$date <- as.Date(sub(" UTC", "", dataset$created_at_tz))

  dataset$idea <- gsub("(.{1,80})(\\s|$)", "\\1\n", dataset$idea)

  return(dataset)
}

africa_coord_date <- function(dataset) {
  dataset <- dplyr::left_join(dataset,
    africa_locations,
    by = c(
      "country_name", "location_name",
      "service_point_name"
    ),
    all.x = TRUE,
    sort = FALSE
  )
  dataset$date <- as.Date(sub(" UTC", "", dataset$created_at_tz))
  dataset$idea <- gsub("(.{1,80})(\\s|$)", "\\1\n", dataset$idea)
  return(dataset)
}


#####################################
#
#   for local mode, not default
#
####################################

basic_africa_local <- function() {
  rv$dataset <- africa_feed_con_raw_03_coords
  rv$dataset <- rv$dataset[which(rv$dataset$country_name %in% input$africa_country_input &
    rv$dataset$location_name %in% input$center_input), ]

  if (nrow(rv$dataset) < sel_reac()) {
    sel <- nrow(rv$dataset)
  } else {
    sel <- sel_reac()
  }

  if (input$old_new_input == 1) {
    rv$dataset <- rv$dataset[1:sel, ]
  } else {
    rv$dataset <- rv$dataset[(nrow(rv$dataset) - sel):nrow(rv$dataset), ]
  }
}

##################################
#
#   local mode is disabled by default
#
###################################

basic_colombia_local <- function() {
  center_init <- "Cundinamarca"

  dataset <- col_feed_con_raw_01_coords
  dataset <- dataset[which(dataset$state %in% center_init), ]

  if (nrow(dataset) < 1500) {
    sel <- nrow(dataset)
  } else {
    sel <- 1500
  }
  # if(input$old_new_input==1){
  #   dataset<-dataset[1:sel,]
  # } else {
  dataset <- dataset[nrow(dataset):(nrow(dataset) - (sel - 1)), ]
  # }

  filter_cols <- setdiff(colnames(dataset), unwanted_columns)

  no_idea <- setdiff(filter_cols, "idea")

  dataset <- dataset[, c("idea", no_idea)]
}
