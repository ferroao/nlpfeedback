<img src=app/www/figures/logo.png align="right" width="16%" hspace="50">

# Shiny App. NLP for community feedback<br></br><br></br><br></br><br></br>

The left side menu of the shiny app consists of four pages:

- Input
  - Upload entry to mongoDB
- Main
  - Map
  - Tagging
  - Word cloud
  - Table
- Settings
  - Memory use
- About

The goal of “NLP for community feedback” subset, geolocalize, tag,
summarize in trigrams community feedback.

This app was written in R ([R Core Team, 2021](#ref-R-base)) and also
uses data.table ([Dowle and Srinivasan, 2021](#ref-R-datatable)), dplyr
([Wickham _et al._, 2021](#ref-R-dplyr)) , DT ([Xie _et al._,
2021](#ref-R-DT)), gtools ([Warnes _et al._, 2021](#ref-R-gtools)),
leaflet ([Cheng _et al._, 2021](#ref-R-leaflet)), mongolite ([Ooms,
2014](#ref-R-mongolite)), shiny ([Chang _et al._, 2021](#ref-R-shiny)),
shinydashboard ([Chang and Borges Ribeiro,
2018](#ref-R-shinydashboard)), shinyjs ([Attali, 2020](#ref-R-shinyjs)),
stringr ([Wickham, 2019](#ref-R-stringr))

Most important, it works with python scripts to classify by SVC from
python module sklearn ([sklearn developers, 2021](#ref-sklearn_2021)),
and also with pandas ([The Pandas Development Team,
2021](#ref-pandas_2021)), nltk ([Bird, 2021](#ref-nltk_2021)), numpy
([Travis E. Oliphant et al., 2021](#ref-numpy_2021)), wordcloud
([Andreas Mueller, 2021](#ref-wordcloud_2021))

## Citation

Fernando Roa Ovalle (<froao@unal.edu.co>), Jorge Andrés Alzate Hoyos,
Emanuel Valencia Henao, Juan Andrés Sanabria García, Simón Andrés Ferro
Rodríguez. 2021. Shiny-app, NLP for community feedback.
<a href="https://gitlab.com/ferroao/nlpfeedback"
target="_blank">gitlab.com/ferroao/nlpfeedback</a>

## Disclaimer

Feedback datasets are property of Kuja Kuja and its affiliates and are
not provided here.  
In addition, mongo connections credentials are also not provided in this
repository.

The authors don’t have commercial relationships with Kuja Kuja.  
The authors designed entirely the code (see `notebooks`) and visual
aspect (`app`) of this project.

## R packages

<div id="refs_myrefs">

<div id="ref-R-shinyjs" class="csl-entry">

Attali D. 2020. Shinyjs: Easily improve the user experience of your
shiny apps in seconds. <https://CRAN.R-project.org/package=shinyjs>

</div>

<div id="ref-R-shinydashboard" class="csl-entry">

Chang W, Borges Ribeiro B. 2018. Shinydashboard: Create dashboards with
’shiny’. <https://CRAN.R-project.org/package=shinydashboard>

</div>

<div id="ref-R-shiny" class="csl-entry">

Chang W, Cheng J, Allaire J, Sievert C, Schloerke B, Xie Y, Allen J,
McPherson J, Dipert A, Borges B. 2021. Shiny: Web application framework
for r. <https://CRAN.R-project.org/package=shiny>

</div>

<div id="ref-R-leaflet" class="csl-entry">

Cheng J, Karambelkar B, Xie Y. 2021. Leaflet: Create interactive web
maps with the JavaScript ’leaflet’ library.
<https://CRAN.R-project.org/package=leaflet>

</div>

<div id="ref-R-datatable" class="csl-entry">

Dowle M, Srinivasan A. 2021. Data.table: Extension of ‘data.frame‘.
<https://CRAN.R-project.org/package=data.table>

</div>

<div id="ref-R-mongolite" class="csl-entry">

Ooms J. 2014. The jsonlite package: A practical and consistent mapping
between JSON data and r objects _arXiv:1403.2805 \[stat.CO\]_.
<https://arxiv.org/abs/1403.2805>

</div>

<div id="ref-R-base" class="csl-entry">

R Core Team. 2021. R: A language and environment for statistical
computing. <https://www.R-project.org/>

</div>

<div id="ref-R-gtools" class="csl-entry">

Warnes GR, Bolker B, Lumley T. 2021. Gtools: Various r programming
tools. <https://CRAN.R-project.org/package=gtools>

</div>

<div id="ref-R-stringr" class="csl-entry">

Wickham H. 2019. Stringr: Simple, consistent wrappers for common string
operations. <https://CRAN.R-project.org/package=stringr>

</div>

<div id="ref-R-dplyr" class="csl-entry">

Wickham H, François R, Henry L, Müller K. 2021. Dplyr: A grammar of data
manipulation. <https://CRAN.R-project.org/package=dplyr>

</div>

<div id="ref-R-DT" class="csl-entry">

Xie Y, Cheng J, Tan X. 2021. DT: A wrapper of the JavaScript library
’DataTables’. <https://CRAN.R-project.org/package=DT>

</div>

</div>

## Python modules

<div id="refs_python">

<div id="ref-wordcloud_2021" class="csl-entry">

Andreas Mueller. 2021. Wordcloud.
<https://github.com/amueller/word_cloud>

</div>

<div id="ref-nltk_2021" class="csl-entry">

Bird S. 2021. Nltk. <http://nltk.org/>

</div>

<div id="ref-sklearn_2021" class="csl-entry">

sklearn developers. 2021. Sklearn.
<https://pypi.python.org/pypi/scikit-learn/>

</div>

<div id="ref-pandas_2021" class="csl-entry">

The Pandas Development Team. 2021. Pandas. <https://pandas.pydata.org>

</div>

<div id="ref-numpy_2021" class="csl-entry">

Travis E. Oliphant et al. 2021. Numpy. <https://www.numpy.org>

</div>

</div>

## UI

<figure>
<img src="ui.png" alt="UI" />
</figure>

## MIT License

Copyright 2021 Author’s above

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
“Software”), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
